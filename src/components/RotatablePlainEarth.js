import React, { useState, useEffect, useRef } from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson-client';
// import csvParse from 'csv-parse/lib/sync';
import worldJson from '../world-110m.json';
import countryNames from '../world-country-names.json';

const d3Geo = require('d3-geo');
const d3Transition = require('d3-transition');
const d3Drag = require('d3-drag');
const d3Zoom = require('d3-zoom');

const land = topojson.feature(worldJson, worldJson.objects.land);
const countries = topojson.feature(worldJson, worldJson.objects.countries).features;
// .filter(d => Object.keys(countryNames).some(n => d.id === n.id))
// .sort((a, b) => a.name(b.name));
const borders = topojson.mesh(worldJson, worldJson.objects.countries, (a, b) => a !== b);
const defaultColors = {
  sea: '#14a',
  land: '#99ff99',
  borders: '#339933',
  globeEdge: '#000',
  highlight: '#ff5555',
};
const dragSensitivity = 0.1;
const globe = { type: 'Sphere' };

const getProjection = (width, height, scale = 0.995) =>
  d3Geo
    .geoOrthographic()
    .translate([width / 2, height / 2])
    .scale((width * scale) / 2)
    .clipAngle(90);

const getPathDrawer = (projection, ctx) =>
  d3Geo
    .geoPath()
    .projection(projection)
    .context(ctx);

const drawEarth = (ctx, pathDrawer, width, height, colors) => {
  ctx.clearRect(0, 0, width, height);
  ctx.fillStyle = colors.sea;
  ctx.beginPath();
  pathDrawer(globe);
  ctx.fill();
  ctx.fillStyle = colors.land;
  ctx.beginPath();
  pathDrawer(land);
  ctx.fill();
  ctx.strokeStyle = colors.borders;
  ctx.lineWidth = 0.5;
  ctx.beginPath();
  pathDrawer(borders);
  ctx.stroke();
  // ctx.strokeStyle = colors.globeEdge;
  // ctx.lineWidth = 2;
  // ctx.beginPath();
  // pathDrawer(globe);
  // ctx.stroke();
};

const drawFeature = (ctx, pathDrawer, feature, color) => {
  ctx.fillStyle = color;
  ctx.beginPath();
  pathDrawer(feature);
  ctx.fill();
};

export const RotatablePlainEarth = ({
  width = 960,
  height = 960,
  colors = defaultColors,
  countryHighlight,
  center,
}) => {
  const earthCanvas = useRef();
  const projection = useRef();
  const ctx = useRef();
  const pathDrawer = useRef();
  const dragOrigin = useRef();
  // const [highlight, setHighlight] = useState({
  //   countryIndex: null,
  //   color: null,
  // });
  // const [centerOn, setCenterOn] = useState();
  const [scale, setScale] = useState(1);
  const scaleRef = useRef(scale); // Unfortunate mutable copy, for drag callback to work

  const draw = () => {
    console.log('Draw. country:', countryHighlight);
    drawEarth(ctx.current, pathDrawer.current, width, height, colors);
    if (countryHighlight !== undefined)
      drawFeature(
        ctx.current,
        pathDrawer.current,
        countries.find(obj => obj.id === countryHighlight),
        colors.highlight,
      );
    // drawFeature(ctx.current, pathDrawer.current, countries[countryHighlight], colors.highlight);
  };

  const onDragStart = () => {
    dragOrigin.current = {
      mouse: [d3.event.x, d3.event.y, 0],
      rotation: projection.current.rotate(),
    };
  };

  const onDrag = () => {
    const [ox, oy, oz] = dragOrigin.current.mouse;
    const [rx, ry, rz] = dragOrigin.current.rotation;
    projection.current.rotate([
      rx + ((d3.event.x - ox) * dragSensitivity) / scaleRef.current,
      ry - ((d3.event.y - oy) * dragSensitivity) / scaleRef.current,
      rz,
    ]);
    draw();
  };

  const initDrag = () =>
    d3.select(earthCanvas.current).call(
      d3Drag
        .drag()
        .on('start', onDragStart)
        .on('drag', onDrag),
    );

  const updateProjection = () => {
    console.log('updateProjection Scale:', scale);
    const rot = projection.current && projection.current.rotate();
    projection.current = getProjection(width, height, scale);
    if (rot) projection.current.rotate(rot);
    pathDrawer.current = getPathDrawer(projection.current, ctx.current);
    // initDrag();
    draw();
  };

  const onZoom = () => {
    // console.log('Zoom??', d3.event.transform.k);
    setScale(d3.event.transform.k);
    scaleRef.current = d3.event.transform.k;
    // updateProjection(width, height, d3.event.transform.k);
  };

  useEffect(() => {
    console.log('effect []');
    ctx.current = earthCanvas.current.getContext('2d');
    initDrag();
    d3.select(earthCanvas.current).call(d3Zoom.zoom().on('zoom', onZoom));
  }, []);

  useEffect(
    () => {
      console.log('effect [width, height, colors, scale]');
      // initDrag();
      updateProjection();
    },
    [width, height, colors, scale],
  );

  useEffect(draw, [countryHighlight]);

  useEffect(
    () => {
      const centerOn =
        center !== undefined
          ? countries.find(obj => obj.id === center)
          : countries[Math.floor(Math.random() * countries.length)];
      // setCenterOn(countries[center]);
      console.log('effect [center]', center, centerOn);
      d3Transition
        .transition()
        .duration(1250)
        .tween('rotate', () => {
          const [cx, cy] = d3Geo.geoPath().centroid(centerOn);
          const rotTrace = d3.interpolate(projection.current.rotate(), [-cx, -cy]);
          return tick => {
            projection.current.rotate(rotTrace(tick));
            draw();
          };
        })
        .transition();
    },
    [center],
  );

  return (
    <div>
      <canvas ref={earthCanvas} width={width} height={height} />
    </div>
  );
};
