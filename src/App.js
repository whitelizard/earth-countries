import React, { useState, useEffect, useRef } from 'react';
import * as topojson from 'topojson-client';
import { RotatablePlainEarth } from './components/RotatablePlainEarth';
// import countryNames from './world-country-names.json';

import worldJson from './world-110m.json';

// const land = topojson.feature(worldJson, worldJson.objects.land);
const countries = topojson.feature(worldJson, worldJson.objects.countries).features;
// const startIndex = 12;
let ix = 239;
while (!countries.find(obj => obj.id === ix)) ++ix;

export const App = () => {
  const [countryIndex, setCountryIndex] = useState(ix);

  useEffect(
    () => {
      // setTimeout(() => setCountryIndex(countryIndex + 1), 8000);
    },
    [countryIndex],
  );

  // <RotatablePlainEarth />
  return (
    <div>
      <h1>{countryIndex}</h1>
      <RotatablePlainEarth
        center={countryIndex}
        countryHighlight={countryIndex}
        // colors={{ land: '#00f' }}
      />
    </div>
  );
};
